<?php

namespace Yoda\EventBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    /**
     * @Route("/index/{id}/{name}", defaults={"name"="pawan"})
     * @Method({"GET", "POST"})
     */
    public function indexAction($id, $name)
    {
    	$var2 = 'YodaEventBundle.request.uri'; 
    	
    	print_r($id);
    	echo '<br>';
    	print_r($name);
    	echo '<br>';
    	print_r("test output");
    	exit ; 
    }

	/**
     * @Route("/user/{name}", defaults={"name"="pawan"})
     * @Method({"GET"})
     */
    public function myName($name) {
    	return $name;
    }
}
