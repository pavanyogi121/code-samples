<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Department;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DbInsertController extends Controller
{
    /**
     * @Route("department")
     */
    public function DbInsertAction()
    {

    	 $entityManager = $this->getDoctrine()->getManager();
    	 $department = new Department();

    	 $department -> setName('php');
    	 $entityManager->persist($department);
    	 $entityManager->flush();

    	 return new Response('Saved new user with id '.$department->getId());

        // return $this->render('AppBundle:DbInsert:db_insert.html.twig', array(
            // ...
        ));
    }

}
