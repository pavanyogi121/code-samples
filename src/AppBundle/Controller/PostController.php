<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PostController extends Controller
{
    /**
     * @Route("/article/{id}")
     */
    public function showPostAction($id)
    {
        return $this->render('AppBundle:Post:showPost.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/_list-posts/{max}")
     */
    public function getListAction($max)
    {
        return $this->render('AppBundle:Post:list_posts.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/showNew")
     */
    public function showNewAction()
    {
        return $this->render('AppBundle:Post:show_new.html.twig', array(
            // ...
        ));
    }

}
